from rescupy.rescu import Rescu
a2 = 2.818 # lattice constant (ang)
species = [{"label":"Ga", "path":"Ga_AtomicData.mat"}, 
           {"label":"As", "path":"As_AtomicData.mat"}]
fxyz = [0.00,0.00,0.00,
        0.25,0.25,0.25]
atoms = {"fracPositions":fxyz, "speciesIdx":[1,2], "species":species}
inpDict = {"atomSys":{}, "solver": {}}
inpDict["atomSys"]["atoms"] = atoms
inpDict["atomSys"]["cell"] = {"avec":[0.,a2,a2,a2,0.,a2,a2,a2,0.], "res":0.12}
inpDict["atomSys"]["kpoint"] = {"grid":[7,7,7]}
inpDict["atomSys"]["energy"] = {"stressReturn": True}
inpDict["solver"] = {"eig": {"lwork": 2**16}, "mix": {"maxit": 100}}
rscobj = Rescu(inpDict)       # create Rescu object
rscobj.set_cmd({"mpi": "mpiexec -n 1"})
rscobj.write_input("ref_gaas_lcao_scf.json")  # print input file
