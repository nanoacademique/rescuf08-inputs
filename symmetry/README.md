## Release tests requirements: 
Copy pseudopotentials from the directory TM_PBE_Delta of commit b189be0 of git@bitbucket.org:nanoacademique/psdatabase.git
Pseudopotentials list:
- Al_AtomicData.mat
- Ba_AtomicData.mat
- B_AtomicData.mat
- Bi_AtomicData.mat
- Br_AtomicData.mat
- Ca_AtomicData.mat
- Cd_AtomicData.mat
- F_AtomicData.mat
- Ge_AtomicData.mat
- I_AtomicData.mat
- In_AtomicData.mat
- Na_AtomicData.mat
- N_AtomicData.mat
- O_AtomicData.mat
- Rb_AtomicData.mat
- S_AtomicData.mat
- Se_AtomicData.mat
- Si_AtomicData.mat
- Sn_AtomicData.mat
- Sr_AtomicData.mat
- Tl_AtomicData.mat
- Zn_AtomicData.mat
