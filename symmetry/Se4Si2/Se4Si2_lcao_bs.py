import numpy as np
import os
from rescupy.rescu import Rescu
rscobj = Rescu(filename="rsc_Se4Si2_lcao_scf.json")
inpDict = rscobj.asdict()
inpDict["atomSys"]["kpoint"] = {"type":"line"}
rscobj = Rescu(inpDict)
filename = "rsc_Se4Si2_lcao_bs.json"
rscobj.write_input(filename)  
rscobj.set_cmd({'mpi': 'mpiexec -n 1'})      # set               
rscobj.cmd.bs(filename)                      # run rescu_bs
rscobj = Rescu(filename="rescu_bs_out.json") # load results
fig = rscobj.atomSys.plot_bs()               # plot results
fig.savefig("rsc_Se4Si2_lcao_bs.png", dpi=600)     # save fig
