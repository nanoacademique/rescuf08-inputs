from rescupy.rescu import Rescu
a2 = 2.818 # lattice constant (ang)
species = [{"label":"Si", "path":"Si_AtomicData.mat"}] 
fxyz = [
      0.00, 0.00, 0.00,
      0.00, 0.50, 0.50,
      0.50, 0.00, 0.50,
      0.50, 0.50, 0.00,
      0.25, 0.25, 0.25,
      0.25, 0.75, 0.75,
      0.75, 0.25, 0.75,
      0.75, 0.75, 0.25,
      ]
atoms = {"fracPositions":fxyz, "speciesIdx":[1,1,1,1,1,1,1,1], "species":species}
inpDict = {"atomSys":{}, "solver": {}}
inpDict["atomSys"]["atoms"] = atoms
inpDict["atomSys"]["cell"] = {"avec":[5.4307,0.,0.,0.,5.4307,0.,0.,0.,5.4307], "res":0.22}
inpDict["atomSys"]["kpoint"] = {"grid":[1,1,1]}
inpDict["atomSys"]["energy"] = {"stressReturn": True}
inpDict["solver"] = {"eig": {"lwork": 2**16}, "mix": {"maxit": 100}}
rscobj = Rescu(inpDict)       # create Rescu object
rscobj.set_cmd({"mpi": "mpiexec -n 1"})
rscobj.write_input("ref_si_cub_x01_y01_z01.json")  # print input file
