# RESCUF08 integration tests

## Organisation

The integration tests are organized in various suites. 
This allows running subsets of the entire integration test base to cover particular features. 
For instance, to test the basic calculator (ground state), for crystals of various symmetries, one may perform the _symmetry_ test suite.

Each test is named using the following convention. 
1. Always start with the tag `ref`.
2. Then name your test, for example `gaas`.
3. Then add a basis tag: `lcao`, `pw`, `real`.
4. Finally, add a calculation tag: `bs`, `dos`, `scf`. 
The tags are separated using underscores `_`, for example a valid name is `ref_gaas_lcao_scf`. 
The extension is of course JSON.

The validate script will parse and use parts of the file name to decide what to check. 
It is therefore important to comply with the rules.

## Create a new test

Test should be generated using `rescupy`.
Suppose I would like to add a test for the ground state of aluminium. 
I first create Python script `al_lcao_scf.py`; I am assuming an NAO basis.
The script should create the input file for an aluminum crystal called `ref_al_lcao_scf.json`. 
Note that for total energy calculations, it is better to set `frcReturn` and `stressReturn` to true, to test more quantities.
Next, I must perform the calculation using a known version of rescuf08, which will spit out `rescu_scf_out.json`. 
The resulting file will contain the same information as `ref_al_lcao_scf.json` plus some results pertaining the energetics (e.g. total energy, forces, stress, etc.)
I then copy `rescu_scf_out.json` back to `ref_al_lcao_scf.json`, effectively updating it.
I add `ref_al_lcao_scf.json` to the Git staging area for recording, along with `al_lcao_scf.py`. 
I also add the _main_ tag, `al_lcao_scf` to the file `testlist.txt`, which contains the tests to be run in the suite. 
The file exists in case one would like to keep tests without necessarily running them. 
Finally, I commit and tag the rescuf08 version used for the tests as follows: 
```
git tag -a ref-#-SHA -m 'message'
git push
git push origin --tags
```
Here # stands for a number allowing to keep track of updates coming from the same rescuf08 version. 
Start with 1, and increasing by 1 until reaching an available tag.
Never overwrite existing tags.
SHA is the hash of the git revision used to create the reference results.
