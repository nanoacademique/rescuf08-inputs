import numpy as np
import os
from rescupy.rescu import Rescu
rscobj = Rescu(filename="rsc_F4In4O4_lcao_scf.json")
inpDict = rscobj.asdict()
inpDict["atomSys"]["kpoint"] = {"grid":[11,11,11]}
inpDict["atomSys"]["pop"]["type"] = "tm"
rscobj = Rescu(inpDict)
filename = "rsc_F4In4O4_lcao_dos.json"
rscobj.write_input(filename)  
rscobj.set_cmd({'mpi': 'mpiexec -n 1'})      # set               
rscobj.cmd.dos(filename)                      # run rescu_dos
rscobj = Rescu(filename="rescu_dos_out.json") # load results
fig = rscobj.atomSys.dos.plot_dos()               # plot results
fig.savefig("rsc_F4In4O4_lcao_dos.png", dpi=600)     # save fig
